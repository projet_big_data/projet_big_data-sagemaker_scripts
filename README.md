# projet_big_data-sagemaker_scripts

Repository de l'instance des notebooks de AWS Sagemaker sur laquelle est exécuté périodiquement l'application de prédicition.

Le Git du notebook est automatiquement cloné sur l'instance Sagemaker lors du déploiement avec Terraform. Les modifications de la branche `master` du Git sont ainsi répercutée directement en production dans Sagemaker.

## Automatisation

L'automatisation de l'exécution du notebook de Sagemaker n'est pas encore en place. Elle est prévue par le biais des applications AWS Lambda et Cloudwatch.

En attendant un noyau acceptant la version python3.8, il est aussi nécessaire de choisir le noyau `conda-python3`.

- [Explication de principe pour l'automatisation de Sagemaker](https://data-sleek.com/automating-aws-sagemaker-notebooks/)
- [Source pour la configration de Lambda avec Terraform](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function)


## Démonstration

- [Vidéo de démonstration d'exécution du script sur Sagemaker](https://youtu.be/yXeOEhjCSGA)